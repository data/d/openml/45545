# OpenML dataset: Tour-and-Travels-Customer-Churn-Prediction

https://www.openml.org/d/45545

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

A Tour & Travels Company Wants To Predict Whether A Customer Will Churn Or Not Based On Indicators Given Below.

Help Build Predictive Models And Save The Company's Money.

Perform Fascinating EDAs.

The Data Was Used For Practice Purposes And Also During A Mini Hackathon, Its Completely Free To Use

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45545) of an [OpenML dataset](https://www.openml.org/d/45545). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45545/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45545/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45545/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

